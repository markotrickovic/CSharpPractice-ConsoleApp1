﻿using System;
using System.Linq;
using System.Diagnostics;

namespace CSharpPractice_ConsoleApp1 {
  [Flags]
  public enum FileAccess {
    // We can define values using integers and the powers of 2:
    Read = 1,
    Write = 2,
    Execute = 4
  }

  [Flags]
  public enum DaysOfWeek {
    // We can also use binary values directly by using the 0b binary prefix.
    // The _ character is used as a separator and is ignored by the compiler
    None = 0b_0000_0000,  // 0
    Monday = 0b_0000_0001,  // 1
    Tuesday = 0b_0000_0010,  // 2
    Wednesday = 0b_0000_0100,  // 4
    Thursday = 0b_0000_1000,  // 8
    Friday = 0b_0001_0000,  // 16
    Saturday = 0b_0010_0000,  // 32
    Sunday = 0b_0100_0000,  // 64
    Weekend = Saturday | Sunday
  }

  public enum LightSwitch {
    Off,
    FiftyPercent,
    Bright,
    Nuclear,
    On
  }

  class Program {
    // Var keyword example
    static void varKeyword() {
      // The var keyword
      var i = 10;
      var someReallayLongVariableName = 9;
      var foo = "Something";
      Console.WriteLine(someReallayLongVariableName);
      var c = 'C';
      // Real Literals
      var myNumber = 4m;
      Console.WriteLine(myNumber.GetType());
    }

    // Type casting
    static void typeCasting() {
      int valueA = 10;
      decimal valueB = valueA;  // Implicit conversion
      Console.WriteLine(valueB);
      decimal valueC = 10;
      //int valueD = valueC;    // This errors out because int cannot be
                                // implicitly converted to by a decimal
      int valueD = (int)valueC; // Explicitly convert valueC to int with the
                                // (int) modifier
      Console.WriteLine(valueD);
    }

    // Example with basic operators
    static void Operators() {
      var apples = 100m;                              // Decimal value
      var oranges = 30m;                              // Decimal value
      Console.WriteLine(apples + oranges);
      Console.WriteLine(apples - oranges);
      Console.WriteLine(apples > oranges);
      Console.WriteLine(apples >= oranges);
      Console.WriteLine(apples < oranges);
      Console.WriteLine(apples <= oranges);
      Console.WriteLine(apples == oranges);
      Console.WriteLine(apples != oranges);           // The not-equals operator
    }

    // Date Types
    static void dateTypes() {
      DateTime today = new DateTime(2021, 2, 4, 5, 1, 0); // Create a date for Februar 1, 2021 at 5:01pm
      Console.WriteLine(today);
      Console.WriteLine(today.Hour);                      // We can reference parts of the DateTime as properties on the variable
      Console.WriteLine(DateTime.Now);  // Display the current local time
      Console.WriteLine(DateTime.MaxValue); // Display the maximum date value
      Console.WriteLine(DateTime.MinValue); // Display the minimum date value
      Console.WriteLine(today.AddDays(7));  // Add a week to Februar 4
      TimeSpan ThreeHours = TimeSpan.FromHours(3);  // Define a TimeSpan of 3 hours
      Console.WriteLine(ThreeHours);  // Show 3 hours as a string
      Console.WriteLine(today.Add(ThreeHours)); // Add 3 hours to 'today' and display the result
    }

    static void Display(MarkoSet<Card> cards) {
      var i = 0;
      Console.WriteLine("index" + "  " + "RankValue" +
                        "  " + "Rank" + "  " + "Suit");
      foreach (var card in cards) {
        Console.WriteLine(i.ToString() + "      " + card.RankValue +
                          "          " + card.Rank + "       " + card.Suit);
        i++;
      }
    }

    static void Display(int i) {
      Console.WriteLine(i);
    }

    static void Display(string s) {
      Console.WriteLine(s);
    }

    static void Display(int[] ii) {
      Console.Write("{ ");
      foreach(var i in ii) {
        Console.Write(i + " ");
      }
      Console.WriteLine("}");
    }

    static int BinarySearchRecursive(int[] sortedArray, int key, int first, int last) {
      if ((first + last) > 0) {
        int mid = (first + last) / 2;
        if (key == sortedArray[mid])
          return mid;
        else if (key < sortedArray[mid])
          return BinarySearchRecursive(sortedArray, key, first, mid - 1);
        else
          return BinarySearchRecursive(sortedArray, key, mid + 1, last);
      }
      return -first;  // not found
    }

    static int BinarySearchIterative(int[] sortedArray, int key, int first, int last) {
      int low = first;
      int high = last;
      while (low <= high) {
        int mid = (low + high) / 2;
        if (key == sortedArray[mid])
          return mid;  // found
        else if (key < sortedArray[mid])
          high = mid - 1;
        else
          low = mid + 1;  // key > sortedArray[mid]
      }
      return -(first);  // not found
    }

    static void BubbleSort(ref int[] array, int last) {
      int ponovo, i, temp;
      do {
        ponovo = 0;
        for (i = 1; i <= last; i++) {
          if (array[i] < array[i - 1]) {
            temp = array[i];
            array[i] = array[i - 1];
            array[i - 1] = temp;
            ponovo = 1;
          }
        }
      } while (ponovo != 0);
    }

    static void Selection(ref int[] array, int n) {
      int i, j, minidx, temp;
      for (i=0; i<n-1; i++) {
        minidx = i;
        for (j=i+1; j<n; j++) {
          if (array[j] < array[minidx]) minidx = j;
        }
        if (i!=minidx) {
          temp = array[i];
          array[i] = array[minidx];
          array[minidx] = temp;
        }
      }
    }

    static void CompactSelection(ref int[] array, int n) {
      int i, j, temp;
      for(i=0;i<n-1;i++) {
        for(j=i+1;j<n;j++) {
          if (array[j]<array[i]) {
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
          }
        }
      }
    }

    static void Insertion(ref int[] array, int n) {
      int i, j, value;
      for (i=0; i<n; i++) {
        value = array[i];
        for (j=i-1; j>=0 && array[j]>value; j--) {
          array[j + 1] = array[j];
        }
        array[j + 1] = value;
      }
    }

    static int Partition(ref int[] array, int l, int r) {
      int pivot, i, j, t;
      pivot = array[l];
      i = l; j = r + l;
      while (true) {
        do ++i; while (array[i] <= pivot && i <= r);
        do --j; while (array[j] > pivot);
        if (i >= j) break;
        t = array[i]; array[i] = array[j]; array[j] = t;
      }
      t = array[l]; array[l] = array[j]; array[j] = t;
      return j;
    }

    static void Quick(ref int[] array, int l, int r) {
      int j;
      if (l < r) {
        j = Partition(ref array, l, r);
        Quick(ref array, l, j - 1);
        Quick(ref array, j + 1, r);
      }
    }

    static void Main(string[] args) {

      Stopwatch watch = Stopwatch.StartNew();

      int[] ints = {24, 8, 15, 31, 2, 9};
      //CompactSelection(ref ints, ints.Length);
      Quick(ref ints, 0, ints.Length);

      Display(ints);
      watch.Stop();

      Console.WriteLine("Time to sort array: {0} seconds", (double)watch.ElapsedMilliseconds / 1000.0);

      watch.Reset();
      watch.Start();
      
      var b = BinarySearchRecursive(ints, 15, 0, 5);
      Display(b);
      watch.Stop();
      Console.WriteLine("Time to search in array recursivley: {0} seconds", (double)watch.ElapsedMilliseconds / 1000.0);

      watch.Reset();
      watch.Start();
      
      var c = BinarySearchIterative(ints, 15, 0, 5);
      Display(c);
      watch.Stop();
      Console.WriteLine("Time to search in array iterativley: {0} seconds", (double)watch.ElapsedMilliseconds / 1000.0);

      BTree d = new BTree();
      d.dodaj(42);
      d.dodaj(15);
      d.dodaj(7);

      int[] x = new int[100];
      int n, i, suma = 0;
      Console.Read();

      //int n = Convert.ToInt32(Console.Read());
      //display(n);


      //      Console.Read(); // End


      // C# 101: First Sessions
      //varKeyword();
      //typeCasting();
      //Operators();
      //dateTypes();

      // C# 101: Classes
      //var s = new Student("Marko", "Trickovic", new DateTime(1993, 5, 12));

      //var TheDeck = new MarkoSet<Card>();
      //TheDeck.Add("A-c").Add("A-d"); TheDeck.Add("A-h"); TheDeck.Add("A-s"); TheDeck.Add("2-c"); TheDeck.Add("2-d"); TheDeck.Add("2-h"); TheDeck.Add("2-s"); TheDeck.Add("3-c"); TheDeck.Add("3-d"); TheDeck.Add("3-h"); TheDeck.Add("3-s"); TheDeck.Add("4-c"); TheDeck.Add("4-d"); TheDeck.Add("4-h"); TheDeck.Add("4-s");
      //TheDeck.Add("5-c"); TheDeck.Add("5-d"); TheDeck.Add("5-h"); TheDeck.Add("5-s"); TheDeck.Add("6-c"); TheDeck.Add("6-d"); TheDeck.Add("6-h"); TheDeck.Add("6-s"); TheDeck.Add("7-c"); TheDeck.Add("7-d"); TheDeck.Add("7-h"); TheDeck.Add("7-s"); TheDeck.Add("8-c"); TheDeck.Add("8-d"); TheDeck.Add("8-h"); TheDeck.Add("8-s");
      //TheDeck.Add("9-c"); TheDeck.Add("9-d"); TheDeck.Add("9-h"); TheDeck.Add("9-s"); TheDeck.Add("10-c"); TheDeck.Add("10-d"); TheDeck.Add("10-h"); TheDeck.Add("10-s"); TheDeck.Add("J-c"); TheDeck.Add("J-d"); TheDeck.Add("J-h"); TheDeck.Add("J-s");
      //TheDeck.Add("Q-c"); TheDeck.Add("Q-d"); TheDeck.Add("Q-h"); TheDeck.Add("Q-s"); TheDeck.Add("K-c"); TheDeck.Add("K-d"); TheDeck.Add("K-h"); TheDeck.Add("K-s");

      //// TheDeck
      ////display(TheDeck);
      //// The simplest query
      //var outValues = from card in TheDeck
      //                select card;
      //foreach (var c in outValues) {
      //  Console.WriteLine(c);
      //}

      //var results = from card in TheDeck
      //              where card.Suit == "h" && card.RankValue > 10  // Return just the Hearts
      //              orderby card.RankValue descending
      //              select card.Rank;
      //foreach (var c in results) {
      //  Console.WriteLine(c);
      //}

      //Card myCard = new Card("A-h");
      //var res = myCard.ToFormattedValue();
      //Console.WriteLine(res);

    }
  }
}

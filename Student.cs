﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpPractice_ConsoleApp1 {
  public class EnrolledEventArgs : EventArgs {
    public short YearEnrolled { get; set; }
  }

  public class Student {
    public enum EnrolledState {
      NotEnrolled = 0,
      Enrolled = 5,
      OnMentorship = 10,
      Internship = 11,
      MilitaryLeave = 20
    }
    
    public event EventHandler<EnrolledEventArgs> Enrolled;

    public EnrolledState Enroll() {
      // evaluate student's scenario, make a decision
      return EnrolledState.MilitaryLeave;
    }

    // The new BirthDate field is declared with its Type and Access Modifier explicitly
    private DateTime _birthDate;
    private string _firstName;
    private string _lastName;

    public const int MaximumYearsEnrolled = 5;

    public Student() { IsEnrolled = true; }
    public Student(string FirstName, string LastName, DateTime birthDate) : this() {
      this._birthDate = birthDate;
      Console.WriteLine("Maximum years enrolled: " + MaximumYearsEnrolled);
    }

    // Simple publicly accessible properties that define the FirstName,
    //LastName, and Age of our Student
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int Age { get; set; }
    public bool IsEnrolled { get; set; }

    // Enroll and return a success indicator as well as the new StudentId
    public bool Enroll(short year, out string studentId) {
      // do enrollment steps
      studentId = "1234567";
      return true;
    }

    // Grade as a reference type is always passed by reference
    public void CalculateGrade(Grade g) {
      g.FinalScore = 0.9m;

      /*
      g = new Grade {
        NameOfClass = "Calculus",
        FinalScore = 0.75m
      };*/
    }

    public class Grade {
      public string NameOfClass { get; set; }
      public decimal FinalScore { get; set; }
    }

    public void DoSomething(ref int myValue) {
      myValue++;
      Console.WriteLine("Inside of DoSomething myValue is: " + myValue);
    }

    public void DeleteItemsFromShoppingCart(ref int numberOfItems) {
      numberOfItems = 0;
    }

    internal void LeaveClassEarly(DateTime departureTime) {
      // leave class code
    }

    public decimal CalculateGradeForClass(string className) {
      // Let's delegate the calculation to the OTHER implementation
      // of CalculateGradeForClass nad pass this year

      // This method returns a value of type decimal
      return 0.95m;
    }

    public decimal CalculateGradeForClass(string className,
                                          int yearEnrolled) {
      // grade calculation code
      return 0.80m;
    }

    public decimal CalculateGradePointAverage(short enrollmentYear = 2021,
                                              params string[] classes) {
      Console.WriteLine("Calculating GPA for year " + enrollmentYear);
      foreach (var c in classes) {
        Console.WriteLine("Calculating for class " + c);
      }
      return 0.9m;
    }

    public decimal CalculateGradePointAverage(short enrollmentYear = 0) {
      if (enrollmentYear == 0) enrollmentYear = (short)DateTime.Now.Year;
      Console.WriteLine("Calculating GPA for all classes in the year " + enrollmentYear);
      return CalculateGradePointAverage(enrollmentYear, "Art", "History", "Physics");
    }

    public delegate int CalculateHandler(int myArg1, int myArg2);

    public int Calculate(int arg1, int arg2,
                         params CalculateHandler[] handlers) {
      foreach (var handler in handlers) {
        Console.WriteLine(handler(arg1, arg2));
      }

      return 0;
    }

    public int Add(int arg1, int arg2) {
      var output = arg1 + arg2;
      Console.WriteLine("Added: " + output);
      return output;
    }

    public int Substract(int arg1, int arg2) {
      var output = arg1 - arg2;
      Console.WriteLine("Substracted: " + output);
      return output;
    }
  }

  public partial class Teacher {
    public string Name { get; set; }

    public void DisplayAge() {
      GetAge();
    }

    // Definition of the GetAge method
    partial void GetAge();
  }

  public partial class Teacher {
    private DateTime _birthDate;

    public DateTime BirthDate {
      get { return _birthDate; }
      set { _birthDate = value; }
    }

    // Definition of the GetAge method
    partial void GetAge() {
      Console.WriteLine("Get Age: " + (int)DateTime.Now.Subtract(_birthDate).TotalDays / 365);
    }
  }
}

﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSharpPractice_ConsoleApp1 {
  class Card {
    public Card(string def) {
      var values = def.Split('-');
      Rank = values[0];
      Suit = values[1];
    }
    public string Rank;
    public int RankValue {
      get {
        var faceCards = new Dictionary<string, int> { { "J", 1 }, { "Q", 12 }, { "K", 13 }, { "A", 14 } };
        return faceCards.ContainsKey(Rank) ? faceCards[Rank] : int.Parse(Rank);
      }
    }
    public string Suit;
    public override string ToString() {
      return $"{Rank}-{Suit}";
    }

    private static bool IsLegalCardNotation(string notation) {
      var segments = notation.Split('-');
      if (segments.Length != 2) return false;

      var validSuits = new[] { "c", "d", "h", "s" };
      if (!validSuits.Any(s => s == segments[1])) return false;

      var validRanks = new[]
      {"A","2","3","4","5","6","7","8","9","10","J","Q","K"};
      if (!validRanks.Any(r => r == segments[0])) return false;
      return true;
    }

    public static implicit operator Card(string id) {
      if (IsLegalCardNotation(id)) return new Card(id);
      return null;
    }
  }

  static class CSpoz {
    public static string ToFormattedValue(this Card theCard) {
      var outSuit = theCard.Suit == "c" ? "♣" : theCard.Suit == "d" ? "♦" : theCard.Suit == "h" ? "♥" : "♠";
      return theCard.Rank + outSuit;
    }
  }

  class MarkoSet<T> : IEnumerable<T> {
    private List<T> _Inner = new List<T>();

    public IEnumerator<T> GetEnumerator() {
      return _Inner.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
      return _Inner.GetEnumerator();
    }

    public MarkoSet<T> Add(T newItem) {
      var insertAt = _Inner.Count == 0 ? 0 : new Random().Next(0, _Inner.Count + 1);
      _Inner.Insert(insertAt, newItem);
      return this;
    }

    public MarkoSet<T> Shuffle() {
      _Inner = _Inner.OrderBy(_ => Guid.NewGuid()).ToList();
      return this;
    }
  }
}